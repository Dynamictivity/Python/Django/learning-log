learning_log
------------
An online journal system that lets you keep track of information you’ve learned about particular topics. This project is developed using Django.

# Installation
## Development Setup
1. `python3 -m venv env`
2. `source env/bin/activate`
3. `pip install -r requirements.txt`
4. `python manage.py migrate`
5. `python manage.py runserver`